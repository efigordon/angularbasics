import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-c1',
  templateUrl: './c1.component.html',
  styleUrls: ['./c1.component.css']
})
export class C1Component implements OnInit {

  links = [
    'http://www.efigordon.com',
    'https://www.google.com',
    'http://www.example.com'
  ]
  link: string;
  isTyped: boolean = false;
  inputValArray = [];
  textFromInput = "";
  twoWayInput = "2-Way";
  username = "";
  constructor() { }

  ngOnInit() {
    this.link = this.generateLink();
  }

  generateLink(): string {
    var randNum = Math.round(Math.random() * (this.links.length - 1));
    return this.links[randNum];
  }
  onBtnClick() {
    this.links[2] = "http://othersite.com";
  }
  onUpdateInput(event: any) {
    this.isTyped = true;
    console.log((<HTMLInputElement>event.target).value)
    this.textFromInput = (<HTMLInputElement>event.target).value;
    this.inputValArray = [];
    for (var i = 0; i < this.textFromInput.length; i++) {
      this.inputValArray.push(this.textFromInput[i]);

    }
  }

  onClick2() {
    this.username = "";
  }

  getColor() {
    let d = new Date().getSeconds();
    return d%2 == 0 ? 'LightGreen' : 'PaleTurquoise';
  
  }
}
