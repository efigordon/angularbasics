import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';

import { DeleteMovieComponent } from "./movies/delete-movie.component";

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MyCustomPipe } from "./remove-non-english.pipe";

import { CustomValidator } from "./custom-validator.directive";
import { CustomEditValidator } from './custom-validator-edit.directive';

import { HttpClientModule } from "@angular/common/http";

import { FormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    AddMovieComponent,
    EditMovieComponent,
    DeleteMovieComponent,

    MyCustomPipe,

    CustomValidator,
    CustomEditValidator

  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    DeleteMovieComponent, 
    EditMovieComponent, 
    AddMovieComponent, 
    EditMovieComponent]
})
export class AppModule { }
