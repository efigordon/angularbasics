import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { MovieService } from "./movie.service";

@Directive({
  selector: '[validateUniqueName]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CustomValidator,
      multi: true
    }
  ]
})
export class CustomValidator implements Validator {
  listOfMovies = [];
  validAns : boolean;
  constructor(private movieService: MovieService) {

  }
  ngOnInit() {
    this.listOfMovies = this.movieService.getMovies();
  }
  validate(control: AbstractControl): { [key: string]: any } | null {
  
    this.validAns = true;
    for (let movie of this.listOfMovies) {
      if (movie['Title']==control.value) {
        this.validAns = false;
      }
    }

    return this.validAns ? null: {'defaultSelected': true};

  }
}
