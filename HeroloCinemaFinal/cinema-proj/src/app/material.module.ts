/*
    Here i am manage all the material modules package imports
*/

import { NgModule } from '@angular/core'
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import { MatIconModule, MatFormFieldModule,} from "@angular/material";
//decorator that turns this moudle to be an angular module.
@NgModule({
imports: [
    MatCardModule, 
    MatButtonModule, 
    MatToolbarModule,
     MatDialogModule, 
     MatInputModule, 
     MatIconModule, 
     MatFormFieldModule
    ],
exports: [
    MatCardModule, 
    MatButtonModule, 
    MatToolbarModule,
     MatDialogModule, 
     MatInputModule, 
     MatIconModule, 
     MatFormFieldModule
    ]
}) 
export class MaterialModule {}