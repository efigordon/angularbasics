import { Component, OnInit, OnDestroy } from "@angular/core";
import { ProductsService } from "./products.service";
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-products',
    templateUrl: './products.component.html'
})

export class ProductsComponent implements OnInit, OnDestroy {
    productName : string;
    isDisabled =  true;
    products = [];
    private productsSubscription: Subscription;
    constructor(private productsService: ProductsService) {
        //private bind it to a class property.

       
        setTimeout(() => {
            this.isDisabled = false;
        },2000)
      
    }

    onAddProduct(form) {
        //this.products.push(this.productName);
        //console.log(form); 
        if(form.valid) {
           //  this.products.push(form.value.productName);
            this.productsService.addProduct(form.value.productName);
        }
    }
    onRemoveProduct(product: string) {
        this.products = this.products.filter(p => p!== product);
    }

    ngOnInit() {
        //angular will execute this method right away after the constructor.
        
        this.productsSubscription = this.productsService.productsUpdated.subscribe(() => {
            this.products = this.productsService.getProducts();
        });

    }

    ngOnDestroy() {
        this.productsSubscription.unsubscribe();
    }


} 